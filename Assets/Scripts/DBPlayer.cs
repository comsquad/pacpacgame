using SQLite4Unity3d;

public class DBPlayer  {

	[PrimaryKey, AutoIncrement]
	public int Playerid { get; set; }
	public string Playername { get; set; }
	public int PlayerXP { get; set; }
	public int PlayerLevel {get; set; }

	public override string ToString ()
	{
		return string.Format ("[DBPlayer: Playerid={0}, Playername={1}, PlayerXP={2}, PlayerLevel={3}]", Playerid, Playername, PlayerXP, PlayerLevel);
	}
}