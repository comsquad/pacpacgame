﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextQueue : MonoBehaviour {

    private static ArrayList messageQueue = new ArrayList();
    private static ArrayList animationQueue = new ArrayList(); // really should just rewrite this but we can make it work

    private static List<GameObject> xpQueue;
    private static string toBuild = "";
    private static string currentBuild = "";
    private static int lastPos = 0;
    private static UnityEngine.UI.Text txtObj;
    private UnityEngine.UI.Text downArrowText;
    private static float cd = 0;
    private bool isCurrentMsgFinished = true;
    private static int forceRemove = 2;
    private static float timeToRemove;
    public RectTransform canvasObj;
    public RectTransform actionPanel;
    public RectTransform downArrow;
    private Vector3 orgPos;
    private bool isArrowActive = false;
    public Animations animScript;

    private Color red = new Color(.75f, .1f, .1f);
    private Color white = new Color(1f, 1f, 1f);


    public static void AddMessageToQueue(string m) {
        messageQueue.Add(m);
    }

    public static void ClearTextQueue() {
        messageQueue.Clear();
    }

    public static void AddAnimationToQueue(Hashtable data) {
        //print("Adding to queue @" + (messageQueue.Count));
        if (messageQueue.Count > 2) {
            data["queueLeft"] = messageQueue.Count;
        }
        else {
            data["queueLeft"] = messageQueue.Count - 1;
        }
        animationQueue.Add(data);
    }

    public void SetActionPanelVis(bool isVis) { // Actually isnt hacky this is what people do
        if (!isVis) {
            actionPanel.localScale = new Vector3(0, 0, 0);
        }
        else {
            actionPanel.localScale = new Vector3(1, 1, 1);
        }
    }

    private static void SetTimeToEnd() {
        timeToRemove = Time.realtimeSinceStartup + forceRemove;
    }

	// Use this for initialization
	void Start () {
        txtObj = this.GetComponent<UnityEngine.UI.Text>();
        orgPos = downArrow.localPosition;
        downArrow.gameObject.SetActive(false);
        downArrowText = downArrow.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {

        if (animationQueue.Count > 0) { // probably so many other ways to do this but this is small game so performance won't matter
            Hashtable data = (Hashtable)animationQueue[0];
            int timeToFire = (int)data["queueLeft"];
           // print(timeToFire);
            if (timeToFire == 0) {
                animScript.ProcessAnim(data);
                animationQueue.RemoveAt(0);
            }
        }

        if (messageQueue.Count == 0 && GameManager.isPlayersTurn() && actionPanel.localScale.x == 0) {
            SetActionPanelVis(true);
            txtObj.text = "";
        }else if(messageQueue.Count == 0) {
            if (downArrow.localScale.x > 0) {
                downArrow.localScale = new Vector3(0, 0, 0);
                downArrow.gameObject.SetActive(false);
                isArrowActive = false;
            }
            return;
        }

        if (cd < Time.realtimeSinceStartup) {
            cd = Time.realtimeSinceStartup + .02f;
            if (messageQueue.Count > 0 && currentBuild != (string)messageQueue[0]) {
                currentBuild = (string)messageQueue[0];
                lastPos = 0;
                isCurrentMsgFinished = false;
                SetActionPanelVis(false);
                downArrow.gameObject.SetActive(true);
                downArrow.localScale = new Vector3(1, 1, 1);
                isArrowActive = true;
                downArrow.gameObject.SetActive(true);
            }

            if (lastPos - 1 < currentBuild.Length && !currentBuild.Equals("")) {
                toBuild = currentBuild.Substring(0, lastPos);
                lastPos++;
                txtObj.text = toBuild;
            }
            else if (lastPos - 1 >= currentBuild.Length && !currentBuild.Equals("") && !isCurrentMsgFinished) {
                isCurrentMsgFinished = true;
                SetTimeToEnd();
            }
        }
        if (isCurrentMsgFinished && messageQueue.Count > 0 && (timeToRemove < Time.realtimeSinceStartup || ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) || Input.GetMouseButtonUp(0)))) {
            messageQueue.RemoveAt(0);
            SetTimeToEnd();
            currentBuild = "";
            isArrowActive = false;

            if (animationQueue.Count > 0) {
                for (int k = 0; k < animationQueue.Count; k++){
                    Hashtable tbl = (Hashtable)animationQueue[k];
                    int queueLeft = (int)tbl["queueLeft"];
                    tbl["queueLeft"] = Mathf.Max(0,queueLeft-1);
                    animationQueue[k] = tbl;
                }
            }

        }

        if (isArrowActive) {
            downArrow.transform.localPosition = orgPos + new Vector3(0, Mathf.Sin(Time.realtimeSinceStartup*10) * 5, 0);
            downArrowText.color = Color.Lerp(white, red, Mathf.PingPong(Time.time, 1) * 2);
        }
    }
}
