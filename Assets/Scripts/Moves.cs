﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moves : MonoBehaviour {

    public enum Types {
        NATURE,
        FIRE, 
        WATER,
        ELECTRIC,
    }

    public enum AttackTypes {
        NONE,
        DAMAGE,
        BOOSTD, // Boost Defense
        BOOSTA, // Boost attack
        DECD, // Decrease Defence
        DECA // Decrease Attack
    }

    private static Dictionary<Moves.Types, Dictionary<Moves.Types, int>> Effectiveness = new Dictionary<Moves.Types, Dictionary<Moves.Types, int>>();
    private static Hashtable Attacks = new Hashtable();

    private static Dictionary<Moves.Types, Dictionary<string, Hashtable>> hashedAttacks = new Dictionary<Moves.Types, Dictionary<string, Hashtable>>();
    public static Hashtable typeData = new Hashtable();
    private static bool wasInit = false;
    private static System.Random rndm = new System.Random();


    public static void Start() {
        if (wasInit) { return; }
        wasInit = true;
        AddTypeNames();
        AddEffective();
        AddMoves();
    }

    private static void GenerateMoveTbl(Moves.Types type, string name, int minDmg, int maxDmg, Moves.AttackTypes attackType, int amountOfMoves, AttackTypes extraType = AttackTypes.NONE, int minExta = 0, int maxExtra = 0, bool extraHitsPlayer = false ) {
        Hashtable moveData = new Hashtable();

        moveData["minVal"] = minDmg;
        moveData["maxVal"] = maxDmg;
        moveData["attackType"] = attackType;
        moveData["amountofMoves"] = amountOfMoves;
        moveData["extraAttackType"] = extraType;

        if (extraType != AttackTypes.NONE) {
            moveData["extraMin"] = minExta;
            moveData["extraMax"] = maxExtra;
            moveData["extraHitsPlayer"] = extraHitsPlayer;
        }

        Attacks[name] = moveData;
        hashedAttacks[type].Add(name, moveData);
    }

    private static void AddTypeNames() {

        hashedAttacks[Types.ELECTRIC] = new Dictionary<string, Hashtable>();
        hashedAttacks[Types.FIRE] = new Dictionary<string, Hashtable>();
        hashedAttacks[Types.NATURE] = new Dictionary<string, Hashtable>();
        hashedAttacks[Types.WATER] = new Dictionary<string, Hashtable>();

        Hashtable electric = new Hashtable();
        Hashtable fire = new Hashtable();
        Hashtable water = new Hashtable();
        Hashtable nature = new Hashtable();

        electric.Add("name", "Electric");
        electric.Add("color", new Color(.1f, .1f, .1f));

        fire.Add("name", "Fire");
        fire.Add("color", new Color(.1f, .1f, .1f));

        water.Add("name", "Water");
        water.Add("color", new Color(.1f, .1f, .1f));

        nature.Add("name", "Nature");
        nature.Add("color", new Color(.1f, .1f, .1f));

        typeData[Types.ELECTRIC] = electric;
        typeData[Types.FIRE] = fire;
        typeData[Types.WATER] = water;
        typeData[Types.NATURE] = nature;
    }

    public static Moves.Types GenerateRandomType() {
        Array val = Enum.GetValues(typeof(Types));
        return (Moves.Types)val.GetValue(rndm.Next(val.Length));
    }

    private static void AddMoves() {

        GenerateMoveTbl(Types.NATURE,"Bite", 10,20, AttackTypes.DAMAGE, 25, AttackTypes.DECD, 4,6, false);
        GenerateMoveTbl(Types.NATURE, "Rock Shield", 12,30, AttackTypes.BOOSTD, 3);
        GenerateMoveTbl(Types.NATURE, "Mud Fling", 8,15, AttackTypes.DECA, 3, AttackTypes.DECD, 4,8, false);
        GenerateMoveTbl(Types.NATURE, "Boulder Thow", 14,30, AttackTypes.DAMAGE, 3, AttackTypes.BOOSTA, 2,4, false);

        GenerateMoveTbl(Types.FIRE, "Fireball", 13,15, AttackTypes.DAMAGE, 25);
        GenerateMoveTbl(Types.FIRE, "Solar Flare", 12,18, AttackTypes.DECD, 3, AttackTypes.BOOSTD, 4,6,true);
        GenerateMoveTbl(Types.FIRE, "Fire Dance", 6,8, AttackTypes.BOOSTA, 3, AttackTypes.DAMAGE, 4,6, false);
        GenerateMoveTbl(Types.FIRE, "Blast Burn", 4,18, AttackTypes.DECA, 3, AttackTypes.BOOSTD, 2,9, false);

        GenerateMoveTbl(Types.WATER, "Hydro Cannon", 12,25, AttackTypes.DAMAGE, 25);
        GenerateMoveTbl(Types.WATER, "Rain Dance", 8,18, AttackTypes.BOOSTD, 3, AttackTypes.DECA, 1,3, true);
        GenerateMoveTbl(Types.WATER, "Water Pulse", 10,18, AttackTypes.DECA, 3);
        GenerateMoveTbl(Types.WATER, "Whirlpool", 10,12, AttackTypes.DECD, 3, AttackTypes.DECA, 4,9, false);

        GenerateMoveTbl(Types.ELECTRIC, "Zap", 8,16, AttackTypes.DAMAGE, 25);
        GenerateMoveTbl(Types.ELECTRIC, "Charge Up", 14,19, AttackTypes.BOOSTA, 3, AttackTypes.BOOSTD, 3,6, true);
        GenerateMoveTbl(Types.ELECTRIC, "Discharge", 12,15, AttackTypes.DECD, 3, AttackTypes.DAMAGE, 2,4, true);
        GenerateMoveTbl(Types.ELECTRIC, "Bolt Strike", 18,21, AttackTypes.DAMAGE, 2);
    }

    private static void AddEffective() {
        Dictionary<Moves.Types, int> nature = new Dictionary<Moves.Types, int>();
        Dictionary<Moves.Types, int> fire = new Dictionary<Moves.Types, int>();
        Dictionary<Moves.Types, int> water = new Dictionary<Moves.Types, int>();
        Dictionary<Moves.Types, int> electiric = new Dictionary<Moves.Types, int>();

        nature.Add(Types.NATURE, 1);
        nature.Add(Types.FIRE, 0);
        nature.Add(Types.WATER, 2);
        nature.Add(Types.ELECTRIC, 1);

        fire.Add(Types.NATURE, 2);
        fire.Add(Types.FIRE, 1);
        fire.Add(Types.WATER, 0);
        fire.Add(Types.ELECTRIC, 1);

        water.Add(Types.NATURE, 1);
        water.Add(Types.FIRE, 2);
        water.Add(Types.WATER, 1);
        water.Add(Types.ELECTRIC, 0);

        electiric.Add(Types.NATURE, 0);
        electiric.Add(Types.FIRE, 1);
        electiric.Add(Types.WATER, 2);
        electiric.Add(Types.ELECTRIC, 1);

        Effectiveness.Add(Types.NATURE, nature);
        Effectiveness.Add(Types.FIRE, fire);
        Effectiveness.Add(Types.WATER, water);
        Effectiveness.Add(Types.ELECTRIC, electiric);
    }

    public static Hashtable GetAttackInfo(string name) {
        if (Attacks.Contains(name)) {
            return (Hashtable)Attacks[name];
        }
        else {
            print("We don't have a tbl for " + name);
            return new Hashtable();
        }
    }

    // 0 is not effective
    // 1 is normal
    // 2 is effective
    public static int isMoveEffecive(Moves.Types ourType, Moves.Types theirType) {
        return Effectiveness[ourType][theirType];
    }

    public static string GetTypeName(Moves.Types type) {
        if (typeData.ContainsKey(type)) {
            Hashtable data = (Hashtable)typeData[type];
            return (string)data["name"];
        }
        else {
            return "??";
        }
    }

    public static Color GetTypeColor(Moves.Types type) {
        if (typeData.ContainsKey(type)) {
            Hashtable data = (Hashtable)typeData[type];
            return (Color)data["color"];
        }
        else {
            return new Color(.8f,.1f,.1f);
        }
    }

    public static bool shouldInverse(Moves.AttackTypes type) {
        return type == Moves.AttackTypes.DECA || type == Moves.AttackTypes.DECD;
    }

    public static bool isMoveFriendly(Moves.AttackTypes type) {
        return type == Moves.AttackTypes.BOOSTA || type == Moves.AttackTypes.BOOSTD;
    }

    public static bool isMoveDmg(Moves.AttackTypes type) {
        return type == Moves.AttackTypes.DAMAGE;
    }

    public static bool isMoveDefense(Moves.AttackTypes type) {
        return type == Moves.AttackTypes.BOOSTD || type == Moves.AttackTypes.DECD;
    }

    public static bool isMoveAttack(Moves.AttackTypes type) {
        return type == Moves.AttackTypes.BOOSTA || type == Moves.AttackTypes.DECA;
    }

    // This will support all moves if more are addded
    public static Dictionary<string, int> GenerateMovesByType(Moves.Types type) {
        
        Dictionary<string, Hashtable> movesByType = hashedAttacks[type];

        List<string> attackName = new List<string>(hashedAttacks[type].Keys);

        Dictionary<string, int> toReturn = new Dictionary<string, int>();

        int neededAttacks = GameManager.numOfAttacks;


        //TODO: make while loop if we go over 4 moves per type

        foreach (string s in attackName) {
            Hashtable moveTbl = movesByType[s];

            toReturn.Add(s, (int)moveTbl["amountofMoves"]);
            neededAttacks--;
            if (neededAttacks == 0) { break; }
        }

        return toReturn;

    }
}
