﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDetails : Details {

    public Player player;
    public RectTransform xpBar;
    public RectTransform contentPanel;
    private bool isEarningXP = false;
    private Hashtable moveTbl = new Hashtable();

    private float xpPercent = 0f;

    ArrayList textObj = new ArrayList();
    private int pooledObj = 5;

    public GameObject xpText;

    public void SetNewCreature(string name, int maxhealth, int health, int level, int xp, Moves.Types type) {
        base.SetNewCreature(name, maxhealth, health, level, type);
        xpPercent = xp / (float)player.GetXPRequiredToLevel(level);
        xpBar.transform.localScale = new Vector3(xpPercent, 1, 1);
    }

    // Use this for initialization
    public override void Start() {
        base.Start();
        //xpText.enabled = false;

        for (int k = 0; k < pooledObj; k++) {
            GameObject obj = Instantiate(xpText, contentPanel);
            textObj.Add(obj);
            obj.GetComponent<Text>().enabled = false;
            obj.GetComponent<XPQueue>().queueid = k;
        }
    }

    private GameObject FindInactiveXPText() {
        foreach (GameObject obj in textObj){
            if (!obj.GetComponent<Text>().enabled && !obj.GetComponent<XPQueue>().isTaken) {
                //print("We found object id " + obj.GetComponent<XPQueue>().queueid);
                return obj;
            }
        }
        print("We didn't have enough pooled instances");
        return null;
    }

    private void AddLevelMsg(GameObject obj,int level) {
        obj.GetComponent<XPQueue>().SetLevelMessage(level);
    }

    private void AddXPMsg(GameObject obj, int amount) {
        obj.GetComponent<XPQueue>().SetXPMessage(amount);
    }

    public void SetXPPercent(float percent, int xpGained) {
        xpPercent = percent;
        isEarningXP = true;

        GameObject xpMsg = FindInactiveXPText();

        if (xpMsg != null) {
            AddXPMsg(xpMsg, xpGained);
        }
    }

    public void LevelUpNoVisual() {
        creatureLevel++;
        lvlTxt.text = GetLevelText();
    }

    public void LevelUpVisual() {
        GameObject levelMsg = FindInactiveXPText();
        if (levelMsg != null) {
            AddLevelMsg(levelMsg, creatureLevel);
        }
    }

    public void InitializeNewMoves(Hashtable tbl) {
        moveTbl = tbl;
    }

    public void UpdateMoveCount(Text txt, string move, int index, int amount) {
        Hashtable data = (Hashtable)moveTbl[index];
        data["attacksleft"] = amount;
        txt.text = GetAttackDetails(move, amount, (int)data["maxattacks"]);
    }

    public string GetAttackDetails(string name, int have, int max) {
        return name + "  [" + have.ToString() + "/" + max.ToString() + "]";
    }

    //Todo use coroutines for this

    public override void Update() {
        base.Update();
        if (isEarningXP && xpPercent > xpBar.transform.localScale.x) {
            xpBar.transform.localScale = Vector3.Lerp(new Vector3(xpBar.transform.localScale.x, 1, 1), new Vector3(xpPercent, 1, 1), lerpSpeed);
            if (xpPercent == xpBar.transform.localScale.x) {
                isEarningXP = false;
            }
        } else if (isEarningXP && xpPercent < xpBar.transform.localScale.x) {
            xpBar.transform.localScale = Vector3.Lerp(new Vector3(xpBar.transform.localScale.x, 1, 1), new Vector3(1, 1, 1), lerpSpeed);

            if (xpBar.transform.localScale.x >= .98) {
                xpBar.transform.localScale = new Vector3(0, 1, 1);
                LevelUpVisual();
            }
        }

        if (!isEarningXP && creatureHealth <= 0 && xpBar.transform.localScale.x <= .02) {
            GameManager.manager.onPlayerKilled();
        }
    }
}
