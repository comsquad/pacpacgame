﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Details : MonoBehaviour {

    protected int creatureLevel = 1;
    protected Moves.Types creatureType = Moves.Types.NATURE;
    protected float creatureMaxHealth = 100;
    protected float creatureHealth = 100;
    protected string creatureName = "Unkown";

    public Text lvlTxt;
    public Text typeTxt;
    public Text healthTxt;
    public Text nameTxt;
    public RectTransform healthBar;
    public RectTransform staticHealthBar;

    private Image healthImg;
    private Color defaultHealthColor;

    protected bool isTakingDamage = false;
    protected static float lerpSpeed = .03f;
    private const float div = 255f;

    public void SetNewCreature(string name, int maxhealth, int health, int level, Moves.Types type) {
        isTakingDamage = false;
        creatureName = name;
        creatureLevel = level;
        creatureMaxHealth = maxhealth;
        creatureHealth = health;
        creatureType = type;

        nameTxt.text = creatureName;
        lvlTxt.text = GetLevelText();
        healthTxt.text = GetHealthText();
        healthBar.transform.localScale = new Vector3(health / (float)maxhealth, 1, 1);
        typeTxt.text = Moves.GetTypeName(type);

    }

    public void SetCreatureHealth(float health) {
        isTakingDamage = true;
        creatureHealth = health;
        healthTxt.text = GetHealthText();
    }

    // Use this for initialization
    public virtual void Start () {
        healthImg = healthBar.GetComponent<Image>();
        defaultHealthColor = healthImg.color;
    }

    protected string GetHealthText() {
        return creatureHealth.ToString() + " / " + creatureMaxHealth.ToString();
    }

    protected string GetLevelText() {
        return "Lv " + creatureLevel.ToString();
    }

    public void SetNewLevelStats(int newHealth, int maxHealth) {
        creatureHealth = newHealth;
        creatureMaxHealth = maxHealth;
        healthTxt.text = GetHealthText();
        healthBar.transform.localScale = new Vector3(1, 1, 1);
    }

    private float HealthPercent() {
        return creatureHealth / (float)creatureMaxHealth;
    }

    private bool doesHealthNeedLerp() {
        return healthBar.transform.localScale.x > HealthPercent();
    }

    private Color GetDangerColor() {
        return new Color(Mathf.Abs((Mathf.Sin(Time.realtimeSinceStartup*5) * 200))/ div, 20/ div, 20/ div);
    }

    // Update is called once per frame
    public virtual void Update() {
        if (isTakingDamage && doesHealthNeedLerp() && creatureHealth > 0) {
            healthBar.transform.localScale = Vector3.Lerp(new Vector3(healthBar.transform.localScale.x, 1, 1), new Vector3(HealthPercent(), 1, 1), lerpSpeed);
        }
        else if (isTakingDamage) {
            isTakingDamage = false;
        }

        if (healthBar.transform.localScale.x < .2) {
            healthImg.color = GetDangerColor();
        }
        else if(healthImg.color != defaultHealthColor) {
            healthImg.color = defaultHealthColor;
        }
    }
}
