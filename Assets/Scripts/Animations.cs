﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animations : MonoBehaviour {

    public enum ANIMS {
        TOOKDMG,
        INCDMG,
        INCDEF,
        DECDMG,
        DECDEF,
        NONE,
    }

    public void ProcessAnim(Hashtable data) {

        Animations.ANIMS typeOfAnim = (Animations.ANIMS)data["animType"];
        Creature target = (Creature)data["target"];
        if (typeOfAnim == ANIMS.TOOKDMG) {
            int val = 0;
            if (data.ContainsKey("val")) {
                val = (int)data["val"];
            }
            doDmgAnim(target, val);
            return;
        }else if(typeOfAnim == ANIMS.INCDMG || typeOfAnim == ANIMS.INCDEF) {
            onGained(target, typeOfAnim);
        }
        else if(typeOfAnim == ANIMS.DECDEF || typeOfAnim == ANIMS.DECDMG) {
            onLost(target, typeOfAnim);
        }
    }

	public void doDmgAnim(Creature target,int dmg) {
        target.DMGAnim(dmg);
        target.EmitSound("hit");
    }

    public void onGained(Creature target, ANIMS animType) {
       // print(target);
        target.EmitSound("gained");

        if (animType == ANIMS.INCDEF) {
            target.DefModAnim(true);
        }
        else if(animType == ANIMS.INCDMG) {
            target.DMGModAnim(true);
        }
    }

    public void onLost(Creature target, ANIMS animType) {
       // print(target);
        target.EmitSound("lost");

        if (animType == ANIMS.DECDEF) {
            target.DefModAnim(false);
        }
        else if (animType == ANIMS.DECDMG) {
            target.DMGModAnim(false);
        }

    }
}
