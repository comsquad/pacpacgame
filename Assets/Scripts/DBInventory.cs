using SQLite4Unity3d;

public class DBInventory  {

	public int Playerid { get; set; }
	public int itemid { get; set; }
	public int itemamount { get; set; }

	public override string ToString ()
	{
		return string.Format ("[DBInventory: Playerid={0}, itemid={1},  itemamount={2}]", Playerid, itemid, itemamount);
	}
}