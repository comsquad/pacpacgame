﻿using UnityEngine;
using UnityEngine.UI;

public class Scenes : MonoBehaviour {

    public Button mainToGame;
	public Button loadGame;

    void Start() {
        mainToGame.onClick.AddListener(delegate {
			PlayerPrefs.SetInt("load", 0);
            LoadScene("Main");
        });
		
		loadGame.onClick.AddListener(delegate {
			PlayerPrefs.SetInt("load", 1);
			LoadScene("Main");
		});
    }

	public void LoadScene(string name) {
        UnityEngine.SceneManagement.SceneManager.LoadScene(name);
    }
}
