﻿using UnityEngine;
using System.Collections;
public class AttackPotion : Item
{

    public override void OnUse()
    {
        Player player = GameManager.manager.player;
        float newDmg = (float)((player.GetLevel() * .8) * 2);
        int dmg = Mathf.CeilToInt(newDmg);
        player.DoModifyAttack(player,dmg);
    }
}
